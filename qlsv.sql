5.1)
---
CREATE DATABASE: `qlsv`
--

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) NOT NULL
)
--

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES
('1', 'Hóa dược'),
('2', 'Công nghệ thông tin');
--

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) NOT NULL,
  `TenSV` varchar(15) NOT NULL,
  `GioiTinh` char(1) NOT NULL,
  `NgaySinh` datetime NOT NULL,
  `NoiSinh` varchar(50) NOT NULL,
  `DiaChi` varchar(50) NOT NULL,
  `MaKH` varchar(6) NOT NULL,
  `HocBong` int(11) NOT NULL
) 
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('1', 'Nguyen', 'Thi A', 'Y', '2002-03-04', 'Ha Noi', 'Thanh Xuan', '1', 3),
('2', 'Nguyen ', 'Van B', 'X', '2003-04-05', 'Ha Noi', 'Thach That', '1', 4),
('3', 'Pham', 'Thanh C', 'Y', '2004-06-07', 'Vinh Phuc', 'Vinh Yen', '2', 3),
('4', 'Tong ', 'The D', 'X', '2002-04-06', 'Hai Phong', 'An Lao', '2', 4),
('5', 'Dinh ', 'Viet E', 'X', '2002-04-03', 'Hai Phong', 'Le Chan', '2', 3);
--

ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);
--

ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`MaSV`);
